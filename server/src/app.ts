import express from 'express';
import recipeRouter from './recipe-router';
import ingredientRouter from './ingredient-router';
import favouritesRouter from './favourites-router';
import cartRouter from './cart-router';

/**
 * Express application.
 */
const app = express();

app.use(express.json());

// Since API is not compatible with v1, API version is increased to v2
app.use('/api/v2', recipeRouter);
app.use('/api/v2', ingredientRouter);
app.use('/api/v2', favouritesRouter);
app.use('/api/v2', cartRouter);

export default app;
