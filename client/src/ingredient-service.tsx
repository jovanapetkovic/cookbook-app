import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Ingredient = {
  ingredientId: number;
  ingredientName: string;
  ingredientUnit: string;
};

export type Recipe = {
  recipeId: number;
  recipeDescription: string;
  recipeName: string;
  favouriteCount: number;
};

class IngredientService {
  // get a ingredient with id
  get(ingredientId: number) {
    return axios.get<Ingredient>('/ingredients/' + ingredientId).then((response) => response.data);
  }

  // get all recipes with this ingredient
  getRecipes(ingredientId: number) {
    return axios
      .get<Recipe>('/ingredients/' + ingredientId + '/recipes')
      .then((response) => response.data);
  }

  // get all ingredient
  getAll() {
    return axios.get<Ingredient[]>('/ingredients').then((response) => response.data);
  }

  // create new ingredient
  create(ingredientName: string, ingredientUnit: string) {
    return axios
      .post<{ ingredientId: number }>('/ingredients', {
        ingredientName: ingredientName,
        ingredientUnit: ingredientUnit,
      })
      .then((response) => response.data.ingredientId);
  }

  // update ingredient
  update(ingredient: Ingredient) {
    return axios.put('/ingredients', ingredient).then((response) => response.data);
  }

  // delete ingredient
  delete(ingredientId: number) {
    return axios
      .delete<Ingredient>('/ingredients/' + ingredientId)
      .then((response) => response.data);
  }
}
const ingredientService = new IngredientService();
export default ingredientService;
