import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route } from 'react-router-dom';
import { NavBar, Card, Alert } from './widgets';
import { RecipeDetails, RecipeEdit, RecipeNew, RecipeSearchable } from './recipe-component';
import {
  IngredientList,
  IngredientDetails,
  IngredientNew,
  IngredientUsage,
} from './ingredient-component';
import { FavouritesList } from './favourites-component';
import { CartList } from './cart-component';

class Menu extends Component {
  render() {
    return (
      <NavBar brand="Cookbook">
        <NavBar.Link to="/recipes">Recipes</NavBar.Link>
        <NavBar.Link to="/ingredients">Ingredients</NavBar.Link>
        <NavBar.Link to="/favourites">My Favourites</NavBar.Link>
        <NavBar.Link to="/cart">Shopping Cart</NavBar.Link>
      </NavBar>
    );
  }
}

class Home extends Component {
  render() {
    return <Card title="Welcome">Welcome to Cookbook!</Card>;
  }
}

ReactDOM.render(
  <HashRouter>
    <div>
      <Alert />
      <Menu />
      <Route exact path="/" component={Home} />
      <Route exact path="/recipes" component={RecipeSearchable} />
      <Route exact path="/recipes/:recipeId(\d+)" component={RecipeDetails} />{' '}
      <Route exact path="/recipes/:recipeId(\d+)" component={RecipeEdit} />{' '}
      <Route exact path="/recipes/new" component={RecipeNew} />
      <Route exact path="/ingredients" component={IngredientNew} />
      <Route exact path="/ingredients" component={IngredientList} />
      <Route exact path="/ingredients/:ingredientId(\d+)" component={IngredientDetails} />
      <Route exact path="/ingredients/:ingredientId(\d+)" component={IngredientUsage} />
      <Route exact path="/favourites" component={FavouritesList} />
      <Route exact path="/cart" component={CartList} />
    </div>
  </HashRouter>,
  document.getElementById('root')
);
