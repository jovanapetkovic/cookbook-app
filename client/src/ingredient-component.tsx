import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button } from './widgets';
import { NavLink } from 'react-router-dom';
import ingredientService, { Ingredient } from './ingredient-service';
import { createHashHistory } from 'history';
import { Recipe } from './recipe-service';

const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

/**
 * Renders ingredients.
 */
export class IngredientList extends Component {
  ingredients: Ingredient[] = [];

  ingredientName = '';
  ingredientUnit = '';

  render() {
    return (
      <>
        <Card title="Ingredients">
          {this.ingredients.map((ingredient) => (
            <Row key={ingredient.ingredientId}>
              <Column>
                <NavLink to={'/ingredients/' + ingredient.ingredientId}>
                  {ingredient.ingredientName} {}
                </NavLink>
              </Column>
            </Row>
          ))}
        </Card>
      </>
    );
  }

  mounted() {
    ingredientService
      .getAll()
      .then((ingredients) => (this.ingredients = ingredients))
      .catch((error) => Alert.danger('Error getting Ingredients: ' + error.message));
  }
}

/**
 * Renders form to edit a specific Ingredient.
 */
export class IngredientDetails extends Component<{ match: { params: { ingredientId: number } } }> {
  ingredient: Ingredient = { ingredientId: 0, ingredientName: '', ingredientUnit: '' };

  render() {
    return (
      <>
        <Card title="Details">
          <Row>
            <Column width={2}>{this.ingredient.ingredientName}</Column>
            <Column width={2}>{this.ingredient.ingredientUnit}</Column>
          </Row>
        </Card>
      </>
    );
  }

  mounted() {
    ingredientService
      .get(this.props.match.params.ingredientId)
      .then((ingredient) => (this.ingredient = ingredient))
      .catch((error) => Alert.danger('Error getting Ingredient: ' + error.message));
  }
}

export class IngredientUsage extends Component<{ match: { params: { ingredientId: number } } }> {
  recipe: Recipe = { recipeId: 0, recipeName: '', recipeDescription: '', favouriteCount: 0 };

  render() {
    return (
      <>
        <Card title="Used in Receipes">
          <Row>
            <Column width={2}>{this.recipe.recipeName}</Column>
            <Column width={2}>{this.recipe.recipeDescription}</Column>
          </Row>
        </Card>
      </>
    );
  }

  mounted() {
    ingredientService
      .getRecipes(this.props.match.params.ingredientId)
      .then((recipe) => (this.recipe = recipe))
      .catch((error) => Alert.danger('Error getting Ingredient: ' + error.message));
  }
}

/**
 * Renders form to create new Ingredient.
 */
export class IngredientNew extends Component {
  ingredientName = '';
  ingredientUnit = '';

  handleChange(event: React.ChangeEvent<HTMLSelectElement>) {
    this.ingredientUnit = event.currentTarget.value;
  }

  render() {
    return (
      <>
        <Card title="New Ingredient">
          <Row>
            Name
            <Column width={2}>
              <Form.Input
                type="text"
                value={this.ingredientName}
                onChange={(event) => (this.ingredientName = event.currentTarget.value)}
              />
            </Column>
            Unit
            <Column width={2}>
              <form>
                <select onChange={(event) => this.handleChange(event)}>
                  <option value=""> - not selected -</option>
                  <option value="pc">Piece</option>
                  <option value="g">Gram</option>
                  <option value="ml">Mililiter</option>
                  <option value="pinch">Pinch</option>
                </select>
              </form>
            </Column>
            <Column width={2}>
              <Button.Success
                onClick={() => {
                  ingredientService
                    .create(this.ingredientName, this.ingredientUnit)
                    /*.then((id) => history.push('/ingredients'))*/
                    .catch((error) => Alert.danger('Error creating ingredient: ' + error.message));
                }}
              >
                Create
              </Button.Success>
            </Column>
          </Row>
        </Card>
      </>
    );
  }
}
