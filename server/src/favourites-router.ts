import express from 'express';
import favouritesService from './favourites-service';

// express route
const router = express.Router();

// get all recipes for with that ingredient
router.get('/favourites/:appUserId', (request, response) => {
  const id = Number(request.params.appUserId);
  favouritesService
    .getRecipes(id)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

router.post('/favourites', (request, response) => {
  const data = request.body;
  if (
    data &&
    data.appUserId &&
    data.appUserId.length != 0 &&
    data.recipeId &&
    data.recipeId.length != 0
  ) {
    favouritesService
      .create(data.appUserId, data.recipeId)
      .then((id) => response.send({}))
      .catch((error) => response.status(500).send(error));
  } else {
    response.status(400).send('Missing userId or recipeId');
  }
});

// delete the favourite. it's a composite primary key, so special handling is needed to extract the IDs
router.delete('/favourites/:favouriteIds', (request, response) => {
  const param = request.params.favouriteIds;
  const array = param.split('+');
  const appUserId = array[0];
  const recipeId = array[1];
  favouritesService
    .delete(Number(appUserId), Number(recipeId))
    .then((_result) => response.send())
    .catch((error) => response.status(500).send(error));
});

export default router;
