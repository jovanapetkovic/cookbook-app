import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type CartLine = {
  cartLineId: number;
  ingredientName: string;
  ingredientUnit: string;
  quantity: number;
};

class CartService {
  // get shopping cart
  getCart(appUserId: number) {
    return axios.get<CartLine[]>('/cart/' + appUserId).then((response) => response.data);
  }

  // add item to the cart
  addToCart(appUserId: number, recipeId: number, portions: number) {
    return axios
      .post('/cart/' + appUserId, {
        recipeId: recipeId,
        portions: portions,
      })
      .then((response) => response.data);
  }

  // empty the cart
  emptyCart(appUserId: number) {
    return axios.delete<{}>('/cart/' + appUserId).then((response) => response.data);
  }
}

const cartService = new CartService();
export default cartService;
