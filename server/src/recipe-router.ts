import express from 'express';
import recipeService from './recipe-service';

// express route
const router = express.Router();

router.get('/recipes', (_request, response) => {
  recipeService
    .getAll()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

router.get('/recipes/:recipeId', (request, response) => {
  const id = Number(request.params.recipeId);
  recipeService
    .get(id)
    .then((recipe) =>
      recipe ? response.send(recipe) : response.status(404).send('Recipe not found')
    )
    .catch((error) => response.status(500).send(error));
});

router.post('/recipes', (request, response) => {
  const data = request.body;
  if (data && data.recipeName && data.recipeName.length != 0)
    recipeService
      .create(data.recipeName, data.recipeDescription)
      .then((id) => response.send({ id: id }))
      .catch((error) => response.status(500).send(error));
  else response.status(400).send('Missing recipe name');
});

router.delete('/recipes/:recipeId', (request, response) => {
  recipeService
    .deleteRecipeIngredients(Number(request.params.recipeId))
    .then((_result) => response.send())
    .catch((error) => response.status(200).send(error)); // let's pretend that it's ok that it fails (nothing to delete - no reason for it to throw an error)
  recipeService
    .delete(Number(request.params.recipeId))
    .then((_result) => response.send())
    .catch((error) => response.status(500).send(error));
});

router.put('/recipes', (request, response) => {
  const data = request.body;
  recipeService
    .update(Number(data.recipeId), String(data.recipeName), String(data.recipeDescription))
    .then((_result) => response.send())
    .catch((error) => response.status(500).send(error));
});

router.post('/recipes/:recipeId/addIngredient', (request, response) => {
  const data = request.body;
  recipeService
    .addIngredient(
      Number(request.params.recipeId),
      Number(request.body.ingredientId),
      Number(request.body.quantity)
    )
    .then((_result) => response.send())
    .catch((error) => response.status(500).send(error));
});

// get all ingredients for a specific recipe
router.get('/recipes/:recipeId/ingredients', (request, response) => {
  const id = Number(request.params.recipeId);
  recipeService
    .getIngredients(id)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

export default router;
