import pool from './mysql-pool';
import type { RowDataPacket, ResultSetHeader } from 'mysql2';
import { Ingredient } from './ingredient-service';

export type Recipe = {
  recipeId: number;
  recipeDescription: string;
  recipeName: string;
  favouriteCount: number;
};

export type RecipeIngredient = {
  ingredientId: number;
  ingredientName: number;
  ingredientQuantity: number;
  ingredientUnit: string;
};

class RecipeService {
  // get recipe with id
  get(recipeId: number) {
    return new Promise<Recipe | undefined>((resolve, reject) => {
      pool.query(
        // select recipeId, recipeName, recipeDescription, (select count(*) from favourites where favourites.recipeId = recipe.recipeId) as isFavourite from recipe order by recipeName
        'select recipeId, recipeName, recipeDescription, (select count(*) from favourites where favourites.recipeId = recipe.recipeId) as favouriteCount from recipe where recipeId = ?',
        recipeId,
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results[0] as Recipe);
        }
      );
    });
  }

  // get all recipes
  getAll() {
    return new Promise<Recipe[]>((resolve, reject) => {
      pool.query(
        'select recipeId, recipeName, recipeDescription, (select count(*) from favourites where favourites.recipeId = recipe.recipeId) as favouriteCount from recipe order by recipeName',
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results as Recipe[]);
        }
      );
    });
  }

  // create new recipe with title and description
  create(recipeName: string, recipeDescription: string) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'insert into recipe set recipeName=?, recipeDescription=?',
        [recipeName, recipeDescription],
        (error, results: ResultSetHeader) => {
          if (error) {
            return reject(error);
          }
          resolve(results.insertId);
        }
      );
    });
  }

  // delete recipe with id
  deleteRecipeIngredients(recipeId: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'delete from recipe_ingredient where recipeId = ?',
        [recipeId],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          if (results.affectedRows == 0) return reject(new Error('No row deleted'));

          resolve();
        }
      );
    });
  }

  // delete recipe with id
  delete(recipeId: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'delete from recipe where recipeId = ?',
        [recipeId],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          if (results.affectedRows == 0) return reject(new Error('No row deleted'));

          resolve();
        }
      );
    });
  }

  //Update recipe
  update(recipeId: number, recipeName: string, recipeDescription: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'update recipe set recipeName = ?, recipeDescription = ? where recipeId = ?',
        [recipeName, recipeDescription, recipeId],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          if (results.affectedRows == 0) reject(new Error('No rows updated'));

          resolve();
        }
      );
    });
  }

  //Update recipe
  addIngredient(recipeId: number, ingredientId: number, quantity: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'insert into recipe_ingredient set recipeId = ?, ingredientId = ?, ingredientQuantity = ?',
        [recipeId, ingredientId, quantity],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          if (results.affectedRows == 0) reject(new Error('Could not add ingredient to recipe'));

          resolve();
        }
      );
    });
  }

  // get all ingredients for a specific recipe
  getIngredients(recipeId: number) {
    return new Promise<RecipeIngredient[] | undefined>((resolve, reject) => {
      pool.query(
        'select ingredient.ingredientId, ingredient.ingredientName, ingredient.ingredientUnit, recipe_ingredient.ingredientQuantity from ingredient inner join recipe_ingredient on recipe_ingredient.ingredientId = ingredient.ingredientId where recipe_ingredient.recipeId = ? order by ingredient.ingredientName',
        recipeId,
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);
          resolve(results as RecipeIngredient[]);
        }
      );
    });
  }
}

const recipeService = new RecipeService();
export default recipeService;
