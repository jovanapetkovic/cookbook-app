import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button } from './widgets';
import { NavLink } from 'react-router-dom';
import { createHashHistory } from 'history';
import { Recipe } from './recipe-service';
import favouriteService from './favourites-service';

const history = createHashHistory();

export class FavouritesList extends Component {
  favourites: Recipe[] = [];

  render() {
    return (
      <>
        <Card title="Favourites">
          {this.favourites.map((favourite) => (
            <Row key={favourite.recipeId}>
              <Column width={2}>
                <NavLink to={'/reci0pes/' + favourite.recipeId}>{favourite.recipeName} </NavLink>
              </Column>
              <Column width={2}>
                {
                  <span
                    onClick={() =>
                      favouriteService
                        .delete(1, favourite.recipeId)
                        .then((response) => history.push('/favourites/'))
                    }
                    title="Click to remove from favourites"
                  >
                    &#10060;
                  </span>
                }
              </Column>
            </Row>
          ))}
        </Card>
      </>
    );
  }

  mounted() {
    favouriteService
      .getFavourites(1)
      .then((favourites) => (this.favourites = favourites))
      .catch((error) => Alert.danger('Error getting Favourites: ' + error.message));
  }
}
