import axios from 'axios';
import { Ingredient } from './ingredient-service';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Recipe = {
  recipeId: number;
  recipeDescription: string;
  recipeName: string;
  favouriteCount: number;
};

export type RecipeIngredient = {
  ingredientId: number;
  ingredientName: number;
  ingredientQuantity: number;
  ingredientUnit: string;
};

class RecipeService {
  // get a recipe with id
  get(recipeId: number) {
    return axios.get<Recipe>('/recipes/' + recipeId).then((response) => response.data);
  }

  // get a recipe with id
  getRecipeIngredients(recipeId: number) {
    return axios
      .get<RecipeIngredient[]>('/recipes/' + recipeId + '/ingredients')
      .then((response) => response.data);
  }

  // get all recipes
  getAll() {
    return axios.get<Recipe[]>('/recipes').then((response) => response.data);
  }

  // create new recipe
  create(recipeName: string, recipeDescription: string) {
    return axios
      .post<{ id: number }>('/recipes', {
        recipeName: recipeName,
        recipeDescription: recipeDescription,
      })
      .then((response) => response.data.id);
  }

  // update recipe
  update(recipe: Recipe) {
    return axios.put('/recipes', recipe).then((response) => response.data);
  }

  // delete recipe
  delete(recipeId: number) {
    return axios.delete<Recipe>('/recipes/' + recipeId).then((response) => response.data);
  }

  // add ingredient to recipe
  addIngredient(recipeId: number, ingredientId: number, quantity: number) {
    return axios
      .post('/recipes/' + recipeId + '/addIngredient/', {
        ingredientId: ingredientId,
        quantity: quantity,
      })
      .then((response) => response.data);
  }
}
const recipeService = new RecipeService();
export default recipeService;
