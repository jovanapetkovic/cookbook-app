import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button } from './widgets';
import { NavLink } from 'react-router-dom';
import { createHashHistory } from 'history';
import { Recipe } from './recipe-service';
import cartService, { CartLine } from './cart-service';

const history = createHashHistory();

export class CartList extends Component {
  cartLines: CartLine[] = [];

  render() {
    return (
      <>
        <Card title="Shopping Cart">
          <Row>
            <Column>
              <b>Item</b>
            </Column>
            <Column>
              <b>Qty</b>
            </Column>
            <Column>
              <b>Unit</b>
            </Column>
          </Row>
          {this.cartLines.map((cartLine) => (
            <Row key={cartLine.cartLineId}>
              <Column>{cartLine.ingredientName}</Column>
              <Column>{cartLine.quantity}</Column>
              <Column>{cartLine.ingredientUnit}</Column>
            </Row>
          ))}
          <Row>
            <Column right>
              <Button.Danger
                onClick={() => {
                  cartService
                    .emptyCart(1)
                    .then((id) => history.push('/#'))
                    .catch((error) => Alert.danger('Error emptying cart: ' + error.message));
                }}
              >
                Empty Cart
              </Button.Danger>
            </Column>
          </Row>
        </Card>
      </>
    );
  }

  mounted() {
    cartService
      .getCart(1)
      .then((cartLines) => (this.cartLines = cartLines))
      .catch((error) => Alert.danger('Error getting Favourites: ' + error.message));
  }
}
