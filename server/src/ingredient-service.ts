import pool from './mysql-pool';
import type { RowDataPacket, ResultSetHeader } from 'mysql2';
import { Recipe } from './recipe-service';

export type Ingredient = {
  ingredientId: number;
  ingredientName: string;
  ingredientUnit: string;
};

class IngredientService {
  // get ingredient with id
  get(ingredientId: number) {
    return new Promise<Ingredient | undefined>((resolve, reject) => {
      pool.query(
        'select * from ingredient where ingredientId = ?',
        ingredientId,
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results[0] as Ingredient);
        }
      );
    });
  }

  // get all recipes with this ingredient
  getRecipes(ingredientId: number) {
    return new Promise<Recipe | undefined>((resolve, reject) => {
      pool.query(
        'select * from recipe where recipeId in (select recipeId from recipe_ingredient where ingredientId = ?)',
        ingredientId,
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results[0] as Recipe);
        }
      );
    });
  }

  // get all ingredients
  getAll() {
    return new Promise<Ingredient[]>((resolve, reject) => {
      pool.query(
        'select * from ingredient order by ingredientName',
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);

          resolve(results as Ingredient[]);
        }
      );
    });
  }

  // create new ingredient with name and unit
  create(ingredientName: string, ingredientUnit: string) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'insert into ingredient set ingredientName=?, ingredientUnit=?',
        [ingredientName, ingredientUnit],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);

          resolve(results.insertId);
        }
      );
    });
  }

  // delete ingredient with id
  delete(ingredientId: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'delete from ingredient where ingredientId = ?',
        [ingredientId],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          if (results.affectedRows == 0) return reject(new Error('No row deleted'));

          resolve();
        }
      );
    });
  }

  // update ingredient
  update(ingredientId: number, ingredientName: string, ingredientUnit: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'update ingredient set ingredientName = ?, ingredientUnit = ? where ingredientId = ?',
        [ingredientName, ingredientUnit, ingredientId],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          if (results.affectedRows == 0) reject(new Error('No rows updated'));

          resolve();
        }
      );
    });
  }
}

const ingredientService = new IngredientService();
export default ingredientService;
