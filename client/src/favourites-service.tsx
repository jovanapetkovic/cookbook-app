import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Recipe = {
  recipeId: number;
  recipeDescription: string;
  recipeName: string;
  favouriteCount: number;
};

export type Favourite = {
  appUserId: number;
  recipeId: number;
};

export type Count = {
  counter: number;
};

class FavouritesService {
  // get all recipes with this ingredient
  getFavourites(appUserId: number) {
    return axios.get<Recipe[]>('/favourites/' + appUserId).then((response) => response.data);
  }

  // create new favourite
  create(appUserId: number, recipeId: number) {
    return axios
      .post('/favourites', {
        appUserId: appUserId,
        recipeId: recipeId,
      })
      .then((response) => response.data);
  }

  // delete favourite. composite primary key, special handling needed
  delete(appUserId: number, recipeId: number) {
    const favouriteId = appUserId + '+' + recipeId;
    return axios.delete<Favourite>('/favourites/' + favouriteId).then((response) => response.data);
  }
}

const favouritesService = new FavouritesService();
export default favouritesService;
