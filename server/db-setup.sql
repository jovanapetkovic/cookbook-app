create table app_user (
    appUserId int not null auto_increment,
    firstName text,
    lastName text,
    constraint pk_app_user primary key (appUserId)
);

insert into app_user set firstName = 'Test', lastName = 'User';

create table recipe (
    recipeId int not null auto_increment,
    recipeName text,
    recipeDescription text,
    constraint pk_recipe primary key (recipeId)
    
);

create table ingredient (
    ingredientId int not null auto_increment,
    ingredientName text,
    ingredientUnit text,
    constraint pk_ingredient primary key (ingredientId)
);

create table recipe_ingredient (
    recipeId int,
    ingredientId int,
    ingredientQuantity int,
    constraint pk_recipe_ingredient PRIMARY KEY (recipeId, ingredientId),
    constraint fk_recipe_ingredient_recipe FOREIGN KEY (recipeId) REFERENCES recipe(recipeId),
    constraint fk_recipe_ingredient_ingredient FOREIGN KEY (ingredientId) REFERENCES ingredient(ingredientId)
);

create table favourites (
    appUserId int,
    recipeId int,
    constraint pk_favourites PRIMARY KEY (appUserId, recipeId),
    constraint fk_favourites_user FOREIGN KEY (appUserId) REFERENCES app_user(appUserId),
    constraint fk_favourites_recipe FOREIGN KEY (recipeId) REFERENCES recipe(recipeId)
);

create table cart (
    cartLineId int not null auto_increment,
    appUserId int,
    ingredientId int,
    quantity int,
    constraint pk_cart primary key (cartLineId),
    constraint fk_cart_ingredient foreign key (ingredientId) references ingredient(ingredientId)
);