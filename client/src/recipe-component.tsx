import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button } from './widgets';
import recipeService, { Recipe, RecipeIngredient } from './recipe-service';
import favouritesService from './favourites-service';
import { createHashHistory } from 'history';
import ingredientService, { Ingredient } from './ingredient-service';
import cartService from './cart-service';
import { NavLink } from 'react-router-dom';
const history = createHashHistory(); // Use history.push(...) to programmatically change path, for instance after successfully saving a student

/**
 * Renders recipes.
 */
export class RecipeSearchable extends Component {
  recipes: Recipe[] = [];
  searchTerm: string = '';

  getLink(FavouriteCount: number, appUserId: number, recipeId: number) {
    if (FavouriteCount > 0) {
      return (
        <span
          onClick={() => favouritesService.delete(appUserId, recipeId)}
          title="Click to remove from favourites"
        >
          &#10084;
        </span>
      );
    } else {
      return (
        <span
          onClick={() => favouritesService.create(appUserId, recipeId)}
          title="Click to add to favourites"
        >
          &#129293;
        </span>
      );
    }
  }

  handleChange(event: React.ChangeEvent<HTMLSelectElement>) {
    this.searchTerm = event.currentTarget.value;
  }

  render() {
    return (
      <>
        <Card title="Recipes">
          <div>
            <input
              placeholder="Type to search"
              value={this.searchTerm}
              name="searchTerm"
              onChange={this.handleChange}
            />
            <br />
            <table>
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Description</th>
                  <th> </th>
                </tr>
              </thead>
              <tbody>
                {this.recipes
                  .filter(
                    (recipe) =>
                      recipe.recipeDescription.includes(this.searchTerm) ||
                      recipe.recipeName.includes(this.searchTerm)
                  )
                  .map((recipe) => (
                    <tr key={recipe.recipeId}>
                      <NavLink to={'/recipes/' + recipe.recipeId}>{recipe.recipeName}</NavLink>
                      <td>{recipe.recipeDescription}</td>
                      <td>{this.getLink(recipe.favouriteCount, 1, recipe.recipeId)}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </Card>
        <Button.Success onClick={() => history.push('/recipes/new')}>New Recipe</Button.Success>
      </>
    );
  }

  mounted() {
    recipeService
      .getAll()
      .then((recipes: Recipe[]) => (this.recipes = recipes))
      .catch((error: { message: string }) =>
        Alert.danger('Error getting Recipes: ' + error.message)
      );
  }
}

/**
 * Renders a specific Recipe.
 */
export class RecipeDetails extends Component<{ match: { params: { recipeId: number } } }> {
  recipe: Recipe = { recipeId: 0, recipeName: '', recipeDescription: '', favouriteCount: 0 };
  ingredients: RecipeIngredient[] = [];
  portions: number = 1;

  render() {
    return (
      <>
        <Card title="Recipe Details">
          <Row>
            <Column width={2}>Title:</Column>
            <Column>{this.recipe.recipeName}</Column>
          </Row>
          <Row>
            <Column width={2}>Description:</Column>
            <Column>{this.recipe.recipeDescription}</Column>
          </Row>
          <br />
          <Row>
            <b>Recipe Ingredients:</b>
            {this.ingredients.map((ingredient) => (
              <Row key={ingredient.ingredientId}>
                <Column width={2}>
                  {this.portions * ingredient.ingredientQuantity} {ingredient.ingredientUnit}{' '}
                  {ingredient.ingredientName}
                </Column>
              </Row>
            ))}
          </Row>
          <br />
          <Row>
            Portion
            <Column width={2}>
              <Form.Input
                type="number"
                value={this.portions}
                onChange={(event) => (this.portions = Number(event.currentTarget.value))}
              />
            </Column>
            <Column width={2}>
              <Button.Success
                onClick={() => {
                  cartService
                    .addToCart(1, this.recipe.recipeId, this.portions)
                    .then((id) => history.push('/recipes/' + this.recipe.recipeId))
                    .catch((error) => Alert.danger('Error creating ingredient: ' + error.message));
                }}
              >
                &#128722; Add to Cart
              </Button.Success>
            </Column>
          </Row>
        </Card>
        <br />
      </>
    );
  }

  mounted() {
    recipeService
      .get(this.props.match.params.recipeId)
      .then((recipe) => (this.recipe = recipe))
      .catch((error) => Alert.danger('Error getting Recipe: ' + error.message));

    recipeService
      .getRecipeIngredients(this.props.match.params.recipeId)
      .then((ingredients) => (this.ingredients = ingredients))
      .catch((error) => Alert.danger('Error getting Favourites: ' + error.message));
  }
}

/**
 * Renders form to edit a specific Recipe.
 */
export class RecipeEdit extends Component<{ match: { params: { recipeId: number } } }> {
  recipe: Recipe = { recipeId: 0, recipeName: '', recipeDescription: '', favouriteCount: 0 };
  ingredientId: number = 0;
  qty: number = 0;

  ingredients: Ingredient[] = [];
  ingredientUnit: string = '';

  handleChange(event: React.ChangeEvent<HTMLSelectElement>) {
    this.ingredientId = Number(event.currentTarget.value);
  }

  render() {
    return (
      <>
        <Card title="Edit Recipe">
          <Row>
            <Column width={2}>
              <Form.Label>Title:</Form.Label>
            </Column>
            <Column>
              <Form.Input
                type="text"
                value={this.recipe.recipeName}
                onChange={(event) => (this.recipe.recipeName = event.currentTarget.value)}
              />
            </Column>
          </Row>

          <Row>
            <Column width={2}>
              <Form.Label>Description:</Form.Label>
            </Column>
            <Column>
              <Form.Textarea
                value={this.recipe.recipeDescription}
                onChange={(event) => (this.recipe.recipeDescription = event.currentTarget.value)}
                rows={10}
              />
            </Column>
          </Row>
          <br />
          <Row>
            <Column>
              <Button.Success
                onClick={() => {
                  recipeService.update(this.recipe);
                }}
              >
                Save
              </Button.Success>
              <Button.Danger
                onClick={() => {
                  recipeService.delete(this.recipe.recipeId).then(() => history.push('/Recipes'));
                }}
              >
                Delete
              </Button.Danger>
            </Column>
          </Row>
        </Card>
        <Card title="Add Ingredients">
          <Row>
            Ingredient
            <Column width={4}>
              <form>
                <select onChange={(event) => this.handleChange(event)}>
                  <option value="">- not selected -</option>

                  {this.ingredients.map((ingredient) => (
                    <option key={ingredient.ingredientId} value={ingredient.ingredientId}>
                      {ingredient.ingredientName} ({ingredient.ingredientUnit})
                    </option>
                  ))}
                </select>
              </form>
            </Column>
            Quantity
            <Column width={2}>
              <Form.Input
                type="number"
                value={this.qty}
                onChange={(event) => (this.qty = Number(event.currentTarget.value))}
              />
            </Column>
            <Column width={2}>
              <Button.Success
                onClick={() => {
                  recipeService
                    .addIngredient(this.recipe.recipeId, this.ingredientId, this.qty)
                    .then((id) => history.push('/recipes/' + this.recipe.recipeId))
                    .catch((error) => Alert.danger('Error creating ingredient: ' + error.message));
                }}
              >
                Create
              </Button.Success>
            </Column>
          </Row>
        </Card>
      </>
    );
  }

  mounted() {
    recipeService
      .get(this.props.match.params.recipeId)
      .then((recipe) => (this.recipe = recipe))
      .catch((error) => Alert.danger('Error getting Recipe: ' + error.message));

    ingredientService
      .getAll()
      .then((ingredients) => (this.ingredients = ingredients))
      .catch((error) => Alert.danger('Error getting Ingredients: ' + error.message));
  }
}

/**
 * Renders form to create new Recipe.
 */
export class RecipeNew extends Component {
  recipeName = '';
  recipeDescription = '';

  render() {
    return (
      <>
        <Card title="New Recipe">
          <Row>
            <Column width={2}>
              <Form.Label>Title:</Form.Label>
            </Column>
            <Column>
              <Form.Input
                type="text"
                value={this.recipeName}
                onChange={(event) => (this.recipeName = event.currentTarget.value)}
              />
            </Column>
          </Row>
          <Row>
            <Column width={2}>
              <Form.Label>Description:</Form.Label>
            </Column>
            <Column>
              <Form.Textarea
                value={this.recipeDescription}
                onChange={(event) => (this.recipeDescription = event.currentTarget.value)}
                rows={10}
              />
            </Column>
          </Row>
        </Card>
        <Button.Success
          onClick={() => {
            recipeService
              .create(this.recipeName, this.recipeDescription)
              .then((response) => history.push('/recipes/' + response))
              .catch((error) => Alert.danger('Error creating recipe: ' + error.message));
          }}
        >
          Create
        </Button.Success>
      </>
    );
  }
}
