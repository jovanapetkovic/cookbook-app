import express from 'express';
import cartService from './cart-service';

// express route
const router = express.Router();

router.get('/cart/:appUserId', (request, response) => {
  cartService
    .getCart(Number(request.params.appUserId))
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

router.post('/cart/:appUserId', (request, response) => {
  const data = request.body;
  if (data && data.recipeId && data.recipeId.length != 0)
    cartService
      .addToCart(Number(request.params.appUserId), data.recipeId, data.portions)
      .then((id) => response.send({ id: id }))
      .catch((error) => response.status(500).send(error));
  else response.status(400).send('Missing critical info');
});

router.delete('/cart/:appUserId', (request, response) => {
  cartService
    .emptyCart(Number(request.params.appUserId))
    .then((_result) => response.send())
    .catch((error) => response.status(500).send(error));
});

export default router;
