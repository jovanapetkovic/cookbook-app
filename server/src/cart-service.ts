import pool from './mysql-pool';
import type { RowDataPacket, ResultSetHeader } from 'mysql2';

export type CartLine = {
  cartLineId: number;
  ingredientName: string;
  ingredientUnit: string;
  quantity: number;
};

class CartService {
  // get shopping cart
  getCart(appUserId: number) {
    return new Promise<CartLine[]>((resolve, reject) => {
      pool.query(
        'select cartLineId, quantity, ingredient.ingredientName, ingredient.ingredientUnit from cart inner join ingredient on ingredient.ingredientId = cart.ingredientId where appUserId = ? order by ingredient.ingredientName',
        appUserId,
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);
          resolve(results as CartLine[]);
        }
      );
    });
  }

  // create new recipe with title and description
  addToCart(appUserId: number, recipeId: number, portions: number) {
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'insert into cart (appUserId, ingredientId, quantity) select ?, ingredientId, ingredientQuantity * ? from recipe_ingredient where recipeId = ?',
        [appUserId, portions, recipeId],
        (error, results: ResultSetHeader) => {
          if (error) {
            return reject(error);
          }
          resolve(results.insertId);
        }
      );
    });
  }

  // delete recipe with id
  emptyCart(recipeId: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query('delete from cart', [recipeId], (error, results: ResultSetHeader) => {
        if (error) return reject(error);
        if (results.affectedRows == 0) return reject(new Error('No row deleted'));

        resolve();
      });
    });
  }
}
const cartService = new CartService();
export default cartService;
