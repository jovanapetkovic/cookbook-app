import express from 'express';
import ingredientService from './ingredient-service';

// express route
const router = express.Router();

router.get('/ingredients', (_request, response) => {
  ingredientService
    .getAll()
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

// get all recipes for with that ingredient
router.get('/ingredients/:ingredientId/recipes', (request, response) => {
  const id = Number(request.params.ingredientId);
  ingredientService
    .getRecipes(id)
    .then((rows) => response.send(rows))
    .catch((error) => response.status(500).send(error));
});

router.get('/ingredients/:ingredientId', (request, response) => {
  const id = Number(request.params.ingredientId);
  ingredientService
    .get(id)
    .then((ingredient) =>
      ingredient ? response.send(ingredient) : response.status(404).send('Recipe not found')
    )
    .catch((error) => response.status(500).send(error));
});

router.get('/ingredients/:ingredientId', (request, response) => {
  const id = Number(request.params.ingredientId);
  ingredientService
    .get(id)
    .then((ingredient) =>
      ingredient ? response.send(ingredient) : response.status(404).send('Recipe not found')
    )
    .catch((error) => response.status(500).send(error));
});

router.post('/ingredients', (request, response) => {
  const data = request.body;
  if (data && data.ingredientName && data.ingredientName.length != 0)
    ingredientService
      .create(data.ingredientName, data.ingredientUnit)
      .then((id) => response.send({ id: id }))
      .catch((error) => response.status(500).send(error));
  else response.status(400).send('Missing recipe name');
});

router.delete('/ingredients/:ingredientId', (request, response) => {
  ingredientService
    .delete(Number(request.params.ingredientId))
    .then((_result) => response.send())
    .catch((error) => response.status(500).send(error));
});

router.put('/ingredients', (request, response) => {
  const data = request.body;
  ingredientService
    .update(Number(data.ingredientId), String(data.ingredientName), String(data.ingredientUnit))
    .then((_result) => response.send())
    .catch((error) => response.status(500).send(error));
});

export default router;
