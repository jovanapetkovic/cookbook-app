import pool from './mysql-pool';
import type { RowDataPacket, ResultSetHeader } from 'mysql2';
import { Recipe } from './recipe-service';

export type Favourite = {
  appUserId: number;
  recipeId: number;
};

class IngredientService {
  // get all favourites for this user
  getRecipes(appUserId: number) {
    return new Promise<Recipe[] | undefined>((resolve, reject) => {
      pool.query(
        'select * from recipe where recipeId in (select recipeId from favourites where appUserId = ?) order by recipeName',
        appUserId,
        (error, results: RowDataPacket[]) => {
          if (error) return reject(error);
          resolve(results as Recipe[]);
        }
      );
    });
  }

  // add new recipe as a favourite for a user
  create(appUserId: string, recipeId: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'insert into favourites set appUserId=?, recipeId=?',
        [appUserId, recipeId],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          resolve();
        }
      );
    });
  }

  // delete favourite with composite id
  delete(appUserId: number, recipeId: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'delete from favourites where appUserId=? and recipeId=?',
        [appUserId, recipeId],
        (error, results: ResultSetHeader) => {
          if (error) return reject(error);
          if (results.affectedRows == 0) return reject(new Error('No row deleted'));
          resolve();
        }
      );
    });
  }
}

const ingredientService = new IngredientService();
export default ingredientService;
