# cookbook-app

## Name

Cookbook App Semester project for "Webutvikling" course

## Description

● Det skal være mulig å søke etter, legge til, endre og slette oppskrifter.\
● Antall porsjoner må kunne justeres, og da må også mengden av hver ingrediens i en oppskrift oppdateres.\
● Det skal være mulig å "like" en oppskrift etter at den er registrert.\
● Det skal være enkelt å oppdage oppskrifter. Dette kan f.eks. gjøres gjennom søk, ved å liste opp nye
og populære registreringer, ved å tilby filtrering basert på kategori, land, ingredienser osv.\
● Det skal være mulig å legge oppskriftens ingredienser i en handleliste.\
● Ingredienser må kunne fjernes fra handlelisten i etterkant.\
● Handlelisten må kunne nullstilles.\
● Systemet skal tilby en funksjon som foreslår oppskrifter basert på en eller flere oppgitte ingredienser.\

## Authors

Jovana Petkovic

## TO-DO

● Implement state for ingredients, recipe edit & add recipe ingredient, and add/remove favourite\
● Implement deletion of recipe ingredients\
● Implement searchable table for ingredient usage\
● Expand the objects with relevant fields\
● Style the poor thing

## License

MIT Lincense

## Database Setup

```sh
Run db-setup.sql in the project database
```

## Start server

Install dependencies and start server:

```sh
cd server
npm install
npm start
```

### Run server tests:

```sh
npm test
```

## Bundle client files to be served through server

Install dependencies and bundle client files:

```sh
cd client
npm install
npm start
```

### Run client tests:

```sh
npm test
```
